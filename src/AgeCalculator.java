import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;
import javax.swing.JScrollPane;
import java.sql.*;
import java.awt.SystemColor;
import java.awt.Color;

public class AgeCalculator {

	private JFrame frmMyClinicInfo;
	static private JTable table;

	/**
	 * Launch the application.
	 */
	public static void AgeDetails() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgeCalculator window = new AgeCalculator();
					window.frmMyClinicInfo.setVisible(true);
					window.frmMyClinicInfo.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try 
				{
					Connection con=DataBase.CreateConnection("jdbc:mysql://localhost:3306/myclinic","root","rahul");
					PreparedStatement pst=con.prepareStatement("SELECT lastname,firstname,gender,city,state,birthdate,age,patientagegroup from patientinfo");
					ResultSet rs=pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}
				catch(Exception e3)
				{
					e3.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AgeCalculator() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMyClinicInfo = new JFrame();
		frmMyClinicInfo.getContentPane().setBackground(new Color(72, 209, 204));
		frmMyClinicInfo.setTitle("My Clinic Info");
		frmMyClinicInfo.setBounds(100, 100, 1024, 768);
		frmMyClinicInfo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMyClinicInfo.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("<");
		btnNewButton.setBackground(new Color(255, 255, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HomePage hp=new HomePage();
				frmMyClinicInfo.setVisible(false);
				hp.HPframe.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton.setBounds(10, 33, 60, 30);
		frmMyClinicInfo.getContentPane().add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(125, 128, 775, 394);
		frmMyClinicInfo.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnNewButton_1 = new JButton("Calculate Age & AgeGroup");
		btnNewButton_1.setBackground(new Color(255, 255, 255));
		btnNewButton_1.setForeground(new Color(0, 0, 0));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try
				{
					ResultSet rs=DataBase.ageCal();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}
				catch(Exception e3)
				{
					e3.printStackTrace();
				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_1.setBounds(332, 589, 333, 66);
		frmMyClinicInfo.getContentPane().add(btnNewButton_1);
	}
}
