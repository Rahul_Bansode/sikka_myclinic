import java.awt.EventQueue;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.UIManager;

public class AppointmentDetails {

	private JFrame frmMyClinicInfo;
	public JTextField textField_count;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void ApptDetail() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppointmentDetails window = new AppointmentDetails();
					window.frmMyClinicInfo.setVisible(true);
					window.frmMyClinicInfo.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AppointmentDetails() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMyClinicInfo = new JFrame();
		frmMyClinicInfo.getContentPane().setBackground(UIManager.getColor("Button.background"));
		frmMyClinicInfo.setTitle("My Clinic Info");
		frmMyClinicInfo.setBounds(100, 100, 1024, 768);
		frmMyClinicInfo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMyClinicInfo.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("<");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				HomePage hp=new HomePage();
				frmMyClinicInfo.setVisible(false);
			hp.HPframe.setVisible(true);
				
			}
		});
		btnNewButton.setBounds(24, 32, 60, 30);
		frmMyClinicInfo.getContentPane().add(btnNewButton);
		
		JComboBox comboBox_Mon = new JComboBox();
		comboBox_Mon.setModel(new DefaultComboBoxModel(new String[] {"", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
		comboBox_Mon.setBounds(542, 583, 153, 32);
		frmMyClinicInfo.getContentPane().add(comboBox_Mon);
		
		JComboBox comboBox_Year = new JComboBox();
		comboBox_Year.setModel(new DefaultComboBoxModel(new String[] {"", "2021", "2020", "2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010"}));
		comboBox_Year.setBounds(298, 583, 153, 32);
		frmMyClinicInfo.getContentPane().add(comboBox_Year);
		
		JComboBox comboBox_Clinic = new JComboBox();
		comboBox_Clinic.setModel(new DefaultComboBoxModel(new String[] {"", "noitaroproc latn akkis", "ntryohroproc latn atudgsfs"}));
		comboBox_Clinic.setBounds(61, 584, 153, 30);
		frmMyClinicInfo.getContentPane().add(comboBox_Clinic);
		
		JLabel lblNewLabel = new JLabel("Clinic  :");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBounds(61, 528, 153, 30);
		frmMyClinicInfo.getContentPane().add(lblNewLabel);
		
		JLabel lblFilterAppointmentsBy = new JLabel("Appointment Count By :");
		lblFilterAppointmentsBy.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblFilterAppointmentsBy.setBounds(364, 32, 267, 30);
		frmMyClinicInfo.getContentPane().add(lblFilterAppointmentsBy);
		
		JButton btnNewButton_1 = new JButton("Count");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultSet rs;
				String cbClinic=comboBox_Clinic.getSelectedItem().toString();
				int ClinicID=0;
				if(cbClinic.equals("noitaroproc latn akkis"))
					ClinicID=1;
				else
					ClinicID=2;
				
				try
				{
					int ApptYear=Integer.parseInt(comboBox_Year.getSelectedItem().toString());
					int ApptMonth=Integer.parseInt(comboBox_Mon.getSelectedItem().toString());
					
					 rs=DataBase.appointmentCount(ClinicID, ApptYear, ApptMonth);
					textField_count.setText(Integer.toString(rs.getInt(1)));
					
					rs=DataBase.appointmentInfo(ClinicID, ApptYear, ApptMonth);
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
					
				}
				catch(Exception e2)
				{
					e2.printStackTrace();
				}
			}
		});
		
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_1.setBounds(419, 654, 153, 43);
		frmMyClinicInfo.getContentPane().add(btnNewButton_1);
		
		textField_count = new JTextField();
		textField_count.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_count.setBounds(750, 580, 153, 32);
		frmMyClinicInfo.getContentPane().add(textField_count);
		textField_count.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Appointment Count :");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel_1.setBounds(737, 528, 250, 30);
		frmMyClinicInfo.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Year   :");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel_2.setBounds(298, 527, 153, 32);
		frmMyClinicInfo.getContentPane().add(lblNewLabel_2);
		
		
		
		JLabel lblNewLabel_2_1 = new JLabel("Month :");
		lblNewLabel_2_1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel_2_1.setBounds(542, 527, 153, 32);
		frmMyClinicInfo.getContentPane().add(lblNewLabel_2_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(83, 119, 844, 348);
		frmMyClinicInfo.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}
}
