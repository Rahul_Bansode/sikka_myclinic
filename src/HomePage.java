import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HomePage {

	public JFrame HPframe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePage window = new HomePage();
					window.HPframe.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomePage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		HPframe = new JFrame();
		HPframe.setTitle("My Clinic Info");
		HPframe.setBounds(100, 100, 1024, 768);
		HPframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		HPframe.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		HPframe.setLocationRelativeTo(null);
		
		JButton btnNewButton = new JButton("Appointments By Clinic");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				HPframe.setVisible(false);
				AppointmentDetails.ApptDetail();
			}
		});
		
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 25));
		HPframe.getContentPane().add(btnNewButton);
		
		JButton btnPatientDetails = new JButton("Patients With No Appointment");
		btnPatientDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HPframe.setVisible(false);
				patientDetails.PatDetails();
			}
		});
		
		btnPatientDetails.setFont(new Font("Tahoma", Font.PLAIN, 25));
		HPframe.getContentPane().add(btnPatientDetails);
		
		JButton btnNewButton_2 = new JButton("Calculate Age & AgeGroup");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HPframe.setVisible(false);
				AgeCalculator.AgeDetails();
			}
		});
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 25));
		HPframe.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Production Details");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HPframe.setVisible(false);
				ProductionDetails.ProuctionDetails();
			}
		});
		
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 25));
		HPframe.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Delete Appointment");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HPframe.setVisible(false);
				DeleteAppointment.DeleteApt();
			}
		});
		
		btnNewButton_4.setFont(new Font("Tahoma", Font.PLAIN, 25));
		HPframe.getContentPane().add(btnNewButton_4);
	}
}
