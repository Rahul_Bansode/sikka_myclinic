import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import net.proteanit.sql.DbUtils;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Color;

public class patientDetails {

	private JFrame frmMyClinicInfo;
	static private JTable table;

	/**
	 * Launch the application.
	 */
	public static void PatDetails() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					patientDetails window = new patientDetails();
					window.frmMyClinicInfo.setVisible(true);
					window.frmMyClinicInfo.setLocationRelativeTo(null);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public patientDetails() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMyClinicInfo = new JFrame();
		frmMyClinicInfo.getContentPane().setBackground(new Color(72, 209, 204));
		frmMyClinicInfo.setTitle("My Clinic Info");
		frmMyClinicInfo.setBounds(100, 100, 1024, 768);
		frmMyClinicInfo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMyClinicInfo.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("<");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HomePage hp=new HomePage();
				frmMyClinicInfo.setVisible(false);
				hp.HPframe.setVisible(true);
				
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton.setBounds(10, 34, 60, 30);
		frmMyClinicInfo.getContentPane().add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(113, 139, 766, 485);
		frmMyClinicInfo.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnNewButton_1 = new JButton("Show");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				table.setVisible(true);
				try
				{
					ResultSet rs=DataBase.patientInfo();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}
				catch(Exception e2)
				{
					e2.printStackTrace();
				}
				
			}
		});
		
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_1.setBounds(449, 666, 110, 43);
		frmMyClinicInfo.getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel = new JLabel("Patient Who Do Not Have Appointment In Future");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBounds(235, 54, 561, 53);
		frmMyClinicInfo.getContentPane().add(lblNewLabel);
	}

}
