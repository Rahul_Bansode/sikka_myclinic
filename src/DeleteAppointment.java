import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Color;

public class DeleteAppointment {

	private JFrame frmMyClinicInfo;
	private JTextField txtAmountHere;
	static private JTable table;

	/**
	 * Launch the application.
	 */
	public static void DeleteApt() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeleteAppointment window = new DeleteAppointment();
					window.frmMyClinicInfo.setVisible(true);
					window.frmMyClinicInfo.setLocationRelativeTo(null);
					
					ResultSet rs=DataBase.showDelAptTB();
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DeleteAppointment() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMyClinicInfo = new JFrame();
		frmMyClinicInfo.getContentPane().setBackground(new Color(72, 209, 204));
		frmMyClinicInfo.setTitle("My Clinic Info");
		frmMyClinicInfo.setBounds(100, 100, 1024, 768);
		frmMyClinicInfo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btnNewButton = new JButton("<");
		btnNewButton.setBounds(10, 32, 60, 30);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HomePage hp=new HomePage();
				frmMyClinicInfo.setVisible(false);
				hp.HPframe.setVisible(true);
			}
		});
		frmMyClinicInfo.getContentPane().setLayout(null);
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 25));
		frmMyClinicInfo.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Delete Appointments Having Amount <\r\n");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBounds(73, 593, 442, 55);
		frmMyClinicInfo.getContentPane().add(lblNewLabel);
		
		txtAmountHere = new JTextField();
		txtAmountHere.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtAmountHere.setText("");
			}
		});
		txtAmountHere.setText("Amount Here");
		txtAmountHere.setBounds(539, 593, 96, 55);
		frmMyClinicInfo.getContentPane().add(txtAmountHere);
		txtAmountHere.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("Delete");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int Amount=Integer.parseInt(txtAmountHere.getText());
				
				try
				{
					ResultSet rs=DataBase.deleteAppt(Amount);
					table.setModel(DbUtils.resultSetToTableModel(rs));
					JOptionPane.showMessageDialog(null, "Appointments Deleted Successfully!!","Message", JOptionPane.INFORMATION_MESSAGE);
				}
				catch(Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});
	
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 26));
		btnNewButton_1.setBounds(679, 593, 181, 55);
		frmMyClinicInfo.getContentPane().add(btnNewButton_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(104, 108, 800, 430);
		frmMyClinicInfo.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}

}
