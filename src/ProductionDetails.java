import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import net.proteanit.sql.DbUtils;
import javax.swing.JScrollPane;
import java.awt.Color;

public class ProductionDetails {

	private JFrame frmMyClinicInfo;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void ProuctionDetails() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProductionDetails window = new ProductionDetails();
					window.frmMyClinicInfo.setVisible(true);
					window.frmMyClinicInfo.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ProductionDetails() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMyClinicInfo = new JFrame();
		frmMyClinicInfo.getContentPane().setBackground(new Color(72, 209, 204));
		frmMyClinicInfo.setTitle("My Clinic Info");
		frmMyClinicInfo.setBounds(100, 100, 1024, 768);
		frmMyClinicInfo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMyClinicInfo.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("<");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HomePage hp=new HomePage();
				frmMyClinicInfo.setVisible(false);
				hp.HPframe.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton.setBounds(10, 27, 60, 30);
		frmMyClinicInfo.getContentPane().add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(114, 83, 783, 419);
		frmMyClinicInfo.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel lblClinicName = new JLabel("Clinic Name:");
		lblClinicName.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblClinicName.setBounds(114, 548, 123, 22);
		frmMyClinicInfo.getContentPane().add(lblClinicName);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"", "noitaroproc latn akkis", "ntryohroproc latn atudgsfs"}));
		comboBox_1.setBounds(114, 580, 123, 21);
		frmMyClinicInfo.getContentPane().add(comboBox_1);
		
		JLabel lblProviderName = new JLabel("Provider Name:");
		lblProviderName.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblProviderName.setBounds(323, 548, 123, 22);
		frmMyClinicInfo.getContentPane().add(lblProviderName);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"", "1", "14", "16", "20", "23"}));
		comboBox_2.setBounds(323, 580, 123, 21);
		frmMyClinicInfo.getContentPane().add(comboBox_2);
		
		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setModel(new DefaultComboBoxModel(new String[] {"", "2021", "2020", "2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010"}));
		comboBox_3.setBounds(534, 580, 123, 21);
		frmMyClinicInfo.getContentPane().add(comboBox_3);
		
		JComboBox comboBox_4 = new JComboBox();
		comboBox_4.setModel(new DefaultComboBoxModel(new String[] {"", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
		comboBox_4.setBounds(774, 580, 123, 21);
		frmMyClinicInfo.getContentPane().add(comboBox_4);
		
		JLabel lblYear = new JLabel("Year:");
		lblYear.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblYear.setBounds(534, 548, 123, 22);
		frmMyClinicInfo.getContentPane().add(lblYear);
		
		JLabel lblMonth = new JLabel("Month:");
		lblMonth.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblMonth.setBounds(774, 548, 123, 22);
		frmMyClinicInfo.getContentPane().add(lblMonth);
		
		JButton btnNewButton_1 = new JButton("Produce Result");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cbClinic=comboBox_1.getSelectedItem().toString();
				int ClinicID=0;
				if(cbClinic.equals("noitaroproc latn akkis"))
					ClinicID=1;
				else
					ClinicID=2;
				
				int ProviderName=Integer.parseInt(comboBox_2.getSelectedItem().toString());
				int ProceYear=Integer.parseInt(comboBox_3.getSelectedItem().toString());
				int ProceMonth=Integer.parseInt(comboBox_4.getSelectedItem().toString());
		
				
				try
				{
					ResultSet rs=DataBase.prodDetail(ClinicID, ProviderName, ProceYear, ProceMonth);
					table.setModel(DbUtils.resultSetToTableModel(rs));
				}
				catch(Exception e4)
				{
					e4.printStackTrace();
				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton_1.setBounds(423, 650, 178, 47);
		frmMyClinicInfo.getContentPane().add(btnNewButton_1);
	}
}
