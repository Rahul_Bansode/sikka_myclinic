import java.sql.*;
public class DataBase {

	static  String Url="jdbc:mysql://localhost:3306/myclinic";
	static String User="root";
	static String Pass="rahul";

	/**
	 *This function loads database drivers and creates connection with database 
	 * @param Url Gets Connection url 
	 * @param User Gets mysql User Name 
	 * @param Pass Gets Mysql password
	 * @return Connection object
	 * @throws Exception
	 */
	public static Connection CreateConnection(String Url,String User,String Pass) throws Exception
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con =DriverManager.getConnection(Url,User,Pass);
		con.setAutoCommit(false);
		return con;
	}
	
	/**
	 * This function returns appointment count as peer the given constraints
	 * @param ClinicID Gets Id of a selected clinic
	 * @param ApptYear Gets appointment year
	 * @param ApptMonth Gets appointment month
	 * @return ResultSet Contains count of filtered appointments
	 * @throws Exception
	 */
	public static ResultSet appointmentCount(int ClinicID,int ApptYear,int ApptMonth) throws Exception
	{
		Connection con=CreateConnection(Url,User,Pass);
		PreparedStatement pst=con.prepareStatement("select count(*) "
				+ "from appointmentinfo "
				+ "where clinicid=? AND year(apptdate)=? AND Month(apptdate)=?");
		pst.setInt(1,ClinicID);
		pst.setInt(2,ApptYear);
		pst.setInt(3,ApptMonth);

		ResultSet rs=pst.executeQuery();
		rs.next();
		return rs;
		
	}
	
	
	/**
	 * This function return ResultSet containing appointment information table as per given constrains 
	 * @param ClinicID  Gets Id of a selected clinic
	 * @param ApptYear Gets appointment year
	 * @param ApptMonth Gets appointment month
	 * @return ResultSate Containing tables with Attributes (clinicid patid apptid apptdate  appttime apptlength)
	 * @throws Exception
	 */
	public static ResultSet appointmentInfo(int ClinicID,int ApptYear,int ApptMonth) throws Exception
	{
		Connection con=CreateConnection(Url,User,Pass);
		PreparedStatement pst=con.prepareStatement("select clinicid,apptid,apptdate,appttime,apptlength "
				+ "from appointmentinfo "
				+ "where clinicid=? AND year(apptdate)=? AND Month(apptdate)=?");
		pst.setInt(1,ClinicID);
		pst.setInt(2,ApptYear);
		pst.setInt(3,ApptMonth);
		ResultSet rs=pst.executeQuery();
		return rs;
	}

	
	/**
	 * This function return patient information in ResutStare table 
	 * @return ResultSet containing table with atributes (patid firstname lastname apptdate appttime)
	 * @throws Exception
	 */
	public static ResultSet patientInfo()throws Exception
	{
		Connection con=CreateConnection(Url,User,Pass);
		PreparedStatement pst=con.prepareStatement("select DISTINCT patientinfo.patid,firstname,lastname,apptdate,appttime\r\n"
				+ "from patientinfo\r\n"
				+ "inner join appointmentinfo on patientinfo.patid=appointmentinfo.patid\r\n"
				+ "where appointmentinfo.apptdate > current_date() AND appointmentinfo.appttime > current_time();");
		ResultSet rs=pst.executeQuery();
		return rs;
	}
	
	
	/**
	 * This method Calculates age ,determines age group and returns ResultSet
	 * @return ResultSet containing tables with attributes (lastname firstname gender city state birthdate age patientagegroup)
	 * @throws Exception
	 */
	public static ResultSet ageCal() throws Exception
	{
		Connection con=CreateConnection(Url,User,Pass);
		PreparedStatement pst=con.prepareStatement("Update patientinfo set age=datediff(current_date(),birthdate)/365.25");
		pst.executeUpdate();
		
		pst=con.prepareStatement("update patientinfo set patientagegroup=if(age<18,\"Minor\",\"Adult\")");
		pst.executeUpdate();
				
		pst=con.prepareStatement("SELECT lastname,firstname,gender,city,state,birthdate,age,patientagegroup from patientinfo");
		ResultSet rs=pst.executeQuery();
		return rs;
	}
	
	
	/**
	 * This method filters production details by clinicName by provider name by year by month and returns result table in ResultSet
	 * @param ClinicID Gets Id of a selected clinic
	 * @param ProviderName Gets Provider name in this case its a integer
	 * @param ProceYear Gets Procedure Year
	 * @param ProceMonth Gets procedure Month 
	 * @return ResultSet Containing table with attributes (proceduretype clinicname provider_name Year Month Amounts)
	 * @throws Exception
	 */
	public static ResultSet prodDetail(int ClinicID,int ProviderName,int ProceYear,int ProceMonth)throws Exception
	{
		Connection con=CreateConnection(Url,User,Pass);
		PreparedStatement pst=con.prepareStatement("select distinct transactioninfo.proceduretype,clinicinfo.clinicname,appointmentinfo.provider as provider_name,year(transactioninfo.proceduredate) as Year,month(transactioninfo.proceduredate) as Month,appointmentinfo.amount as Amounts\r\n"
				+ "from appointmentinfo\r\n"
				+ "inner join clinicinfo on clinicinfo.clinicid=appointmentinfo.clinicid\r\n"
				+ "inner join transactioninfo on transactioninfo.clinicid=appointmentinfo.clinicid\r\n"
				+ "where appointmentinfo.clinicid=? AND appointmentinfo.provider=? AND year(transactioninfo.proceduredate)=? AND month(transactioninfo.proceduredate)=?; ");
		pst.setInt(1,ClinicID);
		pst.setInt(2,ProviderName);
		pst.setInt(3,ProceYear);
		pst.setInt(4,ProceMonth);
		
		ResultSet rs=pst.executeQuery();
		return rs;
	}
	
	
	/**
	 * This method deletes the appointments having amount less than input amount 
	 * @param Amount Gets Amount from user
	 * @return ResultSet table containing attributes (patid firstname lastname apptdate appttime amount)
	 * @throws Exception
	 */
	public static ResultSet deleteAppt(int Amount)throws Exception
	{
		Connection con=CreateConnection(Url,User,Pass);
		PreparedStatement pst =con.prepareStatement("delete from appointmentinfo where amount < ?");
		pst.setInt(1,Amount);
		pst.executeUpdate();
		
		pst=con.prepareStatement("select patientinfo.patid,firstname,lastname,apptdate,appttime,amount "
				+ "from patientinfo "
				+ "inner join appointmentinfo on patientinfo.patid=appointmentinfo.patid;");
		ResultSet rs=pst.executeQuery();
		return rs;
		
	}
	
	
	/**
	 * this method displays the required table for delete appointment 
	 * @return ResultSet table containing attributes (patid firstname lastname apptdate appttime amount)
	 * @throws Exception
	 */
	public static ResultSet showDelAptTB()throws Exception
	{
		Connection con=CreateConnection(Url,User,Pass);
		PreparedStatement pst=con.prepareStatement("select patientinfo.patid,firstname,lastname,apptdate,appttime,amount "
				+ "from patientinfo "
				+ "inner join appointmentinfo on patientinfo.patid=appointmentinfo.patid;");
		ResultSet rs=pst.executeQuery();
		return rs;
	}
	
	
	public static void main(String args[])
	{
		
		
	}
}