/*Creating database myclinuic*/
create database myclinic;

/* Using Database*/

use myclinic;

/*Creating table in database Myclinic*/

create table if not exists ClinicInfo (
clinicid int ,
clinicname varchar(255),
city varchar(255),
state varchar(255),
primary key (clinicid));

create table if not exists DoctorInfo (
idno int,
lastname varchar(255),
firstname varchar(255),
city varchar(255),
state varchar(255),
clinicid int,
primary key (idno));


create table if not exists PatientInfo (
patid int ,
practiceid int ,
lastname varchar(255),
firstname varchar(255),
city varchar (255),
state varchar(255),
gender varchar(255),
patientagegroup varchar(255),
birthdate date,
age int,
primary key (patid));

create table if not exists TransactionInfo(
transid int,
patid int,
proceduretype varchar(255),
proceduredate date,
prov int ,
amount float(6),
clinicid int,
primary key(transid));

Create table if not exists AppointmentInfo(
patid int,
apptid int,
apptdate date,
operatory int,
provider int,
appttime time ,
apptlength int,
amount float(6),
clinicid int,
primary key (apptid),
foreign key (clinicid) REFERENCES  clinicinfo(clinicid),
foreign key (patid) REFERENCES  PatientInfo(patid));


/*Load Data From CSV file to MySql Database in Tables*?
 
 /*load clinicinfo.csv file on mysql DB*/
 load data local infile "C:\\Users\\sai1\\Desktop\\Intenship\\Task Details\\clinicinfo.csv" into table clinicinfo
 fields terminated by ','
 lines terminated by '\n'
 ignore 1 lines
 (clinicid,clinicname,city,state);
 
 /*load doctorinfo.csv file on mysql DB*/
 load data local infile "C:\\Users\\sai1\\Desktop\\Intenship\\Task Details\\doctorinfo.csv" into table doctorinfo
 fields terminated by ','
 lines terminated by '\n'
 ignore 1 lines
 (idno,lastname,firstname,city,state,clinicid);
 
 
 /*load patientinfo.csv file on mysql DB*/
 load data local infile "C:\\Users\\sai1\\Desktop\\Intenship\\Task Details\\patientinfo.csv" into table patientinfo
 fields terminated by ','
 lines terminated by '\n'
 ignore 1 lines
 (patid,practiceid,lastname,firstname,city,state,gender,patientagegroup,birthdate,age);
 
 
 /*load transactioninfo.csv file on mysql DB*/
 load data local infile "C:\\Users\\sai1\\Desktop\\Intenship\\Task Details\\transactioninfo.csv" into table transactioninfo
 fields terminated by ','
 lines terminated by '\n'
 ignore 1 lines
 (transid,patid,proceduretype,proceduredate,prov,amount,clinicid);

/*load appointmentinfo.csv file on mysql DB */
load data local infile "C:\\Users\\sai1\\Desktop\\Intenship\\Task Details\\appointmentinfo.csv" into table appointmentinfo 
fields terminated by ','
lines terminated by '\n'
ignore 1 lines
(patid,apptid,apptdate,operatory,provider,appttime,apptlength,amount,clinicid); 








